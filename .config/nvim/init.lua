vim.g.mapleader = ' '

local fn = vim.fn
local execute = vim.api.nvim_command

require "settings"


local install_path = fn.stdpath("data").."/site/pack/packer/opt/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	execute("!git clone https://github.com/wbthomason/packer.nvim "..install_path)
end

vim.cmd [[packadd packer.nvim]]
vim.cmd "autocmd BufWritePost plugins.lua PackerCompile"

require "plugins"

require("lualine").setup{
        options = { theme = "dracula" }        
}

vim.cmd [[colorscheme dracula]]

vim.opt.termguicolors = true
require("bufferline").setup{}

local vimp = require "vimp"

vimp.nnoremap('<leader>b', function()
        vim.cmd [[BufferLineCyclePrev]]
end)

vimp.nnoremap('<leader>n', function()
        vim.cmd [[BufferLineCycleNext]]
end)
vimp.nnoremap('<leader>t', function()
        vim.cmd [[NERDTree]]
end)
