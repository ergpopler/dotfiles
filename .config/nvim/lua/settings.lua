local o = vim.o
local wo = vim.wo
local bo = vim.bo

o.swapfile = true
o.dir = '/tmp'
o.smartcase = true
o.laststatus = 2
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.scrolloff = 12
o.bg = "dark"
o.mouse = "a"
o.title = true
o.encoding = "utf-8"

wo.number = false
wo.wrap = false

bo.expandtab = true
