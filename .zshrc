# This is free and unencumbered software released into the public domain.

export EDITOR=nvim
export VISUAL=nvim
autoload -U compinit colors && compinit && colors
PS1="%B%{$fg[magenta]%}[%{$fg[blue]%}%n%{$fg[green]%}@%{$fg[magenta]%}%M %{$fg[blue]%}%~%{$fg[magenta]%}]%]%{$reset_color%}$%b "

HISTFILE=~/.cache/zsh/history
HISTSIZE=1000
SAVEHIST=1000
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE

# setopt CORRECT_ALL # Autocorrect

zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots)



bindkey -v # Vi mode
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history


source ~/.aliasrc
PATH="$HOME/.local/bin/:$PATH"

source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh >/dev/null 2>&1


